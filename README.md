Ceci est le code-source du site web de Codeurs en Liberté (www.codeursenliberté.fr).

Le site est généré par [Hugo](http://gohugo.io/) à partir de fichiers HTML et Markdown.
Il contient à la fois les pages statiques du site (liste des sociétaires, page « À propos »)
et des articles de blog.

## Comment développer en local

1. [Installer hugo](https://gohugo.io/getting-started/installing/) ;
2. `make run` pour builder le site et lancer un serveur local.

## Comment générer le site

```
make build
```

## Comment déployer le site

Le site est déployé avec [Netlify](https://netlify.com), un service d'hébergement de sites statiques.

Netlify observe automatiquement le dépôt git, et :

- builde et déploie une pré-version pour chaque merge request,
- builde et déploie en production les nouveaux commits mergés dans master.

L'outil de build utilisé (`hugo`) et la version à utiliser (`HUGO_VERSION`) sont configurés dans le
fichier `netlify.toml` à la racine du site. Les autres réglages sont modifiables dans l'interface
d'administration web de Netlify.
